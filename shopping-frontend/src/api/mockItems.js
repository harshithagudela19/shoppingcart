export default [
    {
      id: "0",
      categoryId: "1",
      name: "Almond Toe Court Shoes Patent Black",
      price: 99,
      image: "/uploads/almondblackcourt.jpeg",
      quantity: 5
    },
    {
      id: "1",
      categoryId: "1",
      name: "Suede Shoes, Blue",
      price: 42,
      image: "/uploads/bluesuede.jpg",
      quantity: 4
    },
    {
      id: "2",
      categoryId: "2",
      name: "Leather Driver Saddle Loafers, Tan",
      price: 34,
      image: "/uploads/tanleather.jpeg",
      quantity: 12
    },
    {
      id: "3",
      categoryId: "2",
      name: "Flip Flops, Red",
      price: 19,
      image: "/uploads/flipflopsred.jpeg",
      quantity: 6
    },
    {
      id: "4",
      categoryId: "2",
      name: "Flip Flops, Blue",
      price: 19,
      image: "/uploads/flipflopsblue.jpeg",
      quantity: 0
    },
    {
      id: "5",
      categoryId: "3",
      name: "Gold Button Cardigan, Black",
      price: 167,
      image: "/uploads/goldblackcardigan.jpeg",
      quantity: 6
    },
    {
      id: "6",
      categoryId: "3",
      name: "Cotton Shorts, Medium Red",
      price: 30,
      image: "/uploads/shortsredmed.jpeg",
      quantity: 5
    },
    {
      id: "7",
      categoryId: "4",
      name: "Fine Stripe Short Sleeve Shirt, Grey",
      price: 49.99,
      image: "/uploads/greystripshortsleeve.jpeg",
      quantity: 9
    },
    {
      id: "8",
      categoryId: "4",
      name: "Fine Stripe Short Sleeve Shirt, Green",
      price: 39.99,
      image: "/uploads/greenstripshortsleeve.jpeg",
      quantity: 3
    },
    {
      id: "9",
      categoryId: "5",
      name: "Bird Print Dress, Black",
      price: 270,
      image: "https://m.media-amazon.com/images/I/71ocjpTWXkL._AC_UL320_.jpg",
      quantity: 10
    },
    {
      id: "10",
      categoryId: "5",
      name: "Mid Twist Cut-Out Dress, Pink",
      price: 540,
      image: "https://m.media-amazon.com/images/I/81NCa6n+hZL._AC_UL320_.jpg",
      quantity: 5
    },
    {
      id: "11",
      categoryId: "6",
      name: "Sharkskin Waistcoat, Charcoal",
      price: 75,
      image: "/uploads/sharkskinwaistcoat.jpeg",
      quantity: 2
    },
    {
      id: "12",
      categoryId: "6",
      name: "Lightweight Patch Pocket Blazer, Deer",
      price: 175,
      image: "/uploads/blazerdeer.jpeg",
      quantity: 1
    },
    {
      id: "13",
      categoryId: "5",
      name: "vaidehi creation Women's Silk a-line Dress Material",
      price: 328,
      image: "https://m.media-amazon.com/images/I/61R1tLdX1FL._AC_UL320_.jpg",
      quantity: 10
    },
    {
      id: "14",
      categoryId: "4",
      name: "GLOBALRANG 100% Cotton Checkered Casual Shirt Full Sleeves for Men",
      price: 545,
      image: "https://m.media-amazon.com/images/I/91AX5wRYQ0L._AC_UL320_.jpg",
      quantity: 9
    }
]
  