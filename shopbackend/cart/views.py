from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer

from .models import Product, Order, OrderProduct
from .serializers import ProductSerializer, OrderSerializer, OrderProductSerializer


class IsUser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class ProductListViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductDetailsViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class OrderListViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Order.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OrderDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Order.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OrderProductListViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderProductSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderProduct.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OrderProductDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = OrderProductSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderProduct.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
