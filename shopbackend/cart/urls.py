from django.urls import path

from .views import ProductListViewSet, ProductDetailsViewSet, OrderProductDetailsViewSet, OrderProductListViewSet, OrderListViewSet, OrderDetailsViewSet

urlpatterns = [
    path('products/', ProductListViewSet.as_view({'get': 'list'}), name='productlist'),
    path('product/<pk>',
         ProductDetailsViewSet.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='productdetails'),
    path('orders/', OrderListViewSet.as_view({'get': 'list'}), name='orderlist'),
    path('orders/<str:pk>',
         OrderDetailsViewSet.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderdetails'),
    path('orderproduct/', OrderProductListViewSet.as_view({'get': 'list'}), name='orderproductlist'),
    path('orderproduct/<str:pk>',
         OrderProductDetailsViewSet.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='orderproductdetails'),
]